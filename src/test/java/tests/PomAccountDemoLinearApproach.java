package tests;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import tests.categories.Regression;
import tests.categories.Smoke;

public class PomAccountDemoLinearApproach extends BaseTest {

    @Category(Smoke.class)
    @Test
    public void testEditAccount() {
        loginPage.login(propertyReader.readProperty("username"), propertyReader.readProperty("password"));
        homePage.selectClientsLink();
        clientSearchPage.searchAndSelectFirstClient("Thoughtworks");
        clientPage.editClientDetails("ThoughtworksNew", "twAddressNew");
    }

    @Category(Regression.class)
    @Test
    public void testAddQuotationForClient() {
        loginPage.login(propertyReader.readProperty("username"), propertyReader.readProperty("password"));
        homePage.selectClientsLink();
        clientSearchPage.searchAndSelectFirstClient("Thoughtworks");
        clientPage.addQuotation("Quotation1", "Event1", "01-30-2017");
    }
}
