import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.PomAccountDemoLinearApproach;
import tests.categories.Smoke;

@RunWith(Categories.class)
@Categories.IncludeCategory(Smoke.class)
@Suite.SuiteClasses(PomAccountDemoLinearApproach.class)
public class SmokeSuite {

}
